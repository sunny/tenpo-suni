import compose from 'compose-function'
import { setHours, setMinutes } from 'date-fns/esm/fp'
import h from 'hyperscript'
import { nanoid } from 'nanoid'

import { DATE_H1_HEIGHT, HOUR_HEIGHT } from './constants'

const css = `
:host {
  align-items: center;
  background: #274ab7;
  border-radius: 8px;
  display: flex;
  left: 4.1em;
  padding-left: 8px;
  position: absolute;
  width: calc(100% - 4.1em);
}

:host(.editing) {
  background: #274ab740;
  border: 4px solid #274ab7;
}

:host::after, :host::before {
  --knob-size: 8px;
  background: #212437;
  border: 4px solid #274ab7;
  border-radius: 50%;
  content: '';
  cursor: row-resize;
  height: var(--knob-size);
  pointer-events: none;
  position: absolute;
  width: var(--knob-size);
}

:host::after {
  left: 50%;
  bottom: -10px;
}

:host::before {
  left: 20%;
  top: -10px;
}

.bottom {
  bottom: -6px;
  cursor: row-resize;
  height: 8px;
  left: 0;
  opacity: 0;
  position: absolute;
  width: 100%;
}
`

class DayEvent extends HTMLElement {
  bottomY = 0
  editingBottom = false
  editingTop = false
  height = 60
  lastHeight = 0
  #startDate = new Date()
  #endDate = new Date()

  constructor() {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const style = document.createElement('style')
    style.textContent = css
    shadow.appendChild(style)
    this.render()
  }

  connectedCallback() {
    ;(this.shadowRoot!.host as HTMLElement).style.height = `${this.height}px`
  }

  get endDate() {
    return this.#endDate
  }

  set endDate(date: Date) {
    this.#endDate = date
    this.setAttribute('end-date', date.toISOString())
  }

  get startDate() {
    return this.#startDate
  }

  set startDate(date: Date) {
    this.#startDate = date
    this.setAttribute('start-date', date.toISOString())
  }

  ontouchstart = () => {
    this.editingBottom = true
    const { bottom: bottomY } = this.getBoundingClientRect()
    this.bottomY = bottomY
  }

  ontouchmove = (e: TouchEvent) => {
    e.preventDefault()
    const touchY = e.changedTouches[0].clientY
    const newHeight =
      Math.ceil((this.height + (touchY - this.bottomY)) / 30) * 30

    if (newHeight === this.lastHeight) return

    this.lastHeight = newHeight
    ;(this.shadowRoot!.host as HTMLElement).style.height = `${newHeight}px`
    // this.endTime = null
    navigator.vibrate(75)
  }

  ontouchend = (e: TouchEvent) => {
    const touchY = e.changedTouches[0].clientY
    this.height = Math.ceil((this.height + (touchY - this.bottomY)) / 30) * 30
  }

  render() {
    this.shadowRoot!.append(
      h('div.bottom', {
        ontouchstart: this.ontouchstart,
        ontouchmove: this.ontouchmove,
        ontouchend: this.ontouchend,
      }),
      h('div.top'),
    )
  }
}

customElements.define('day-event', DayEvent)

export const createEvent = (hour: number, minutes: number, date: Date) => {
  const event = document.createElement('day-event') as DayEvent
  event.classList.add('editing')
  event.id = nanoid()
  event.style.top = `calc(${DATE_H1_HEIGHT}rem + ${
    (hour + minutes / 60) * HOUR_HEIGHT
  }px)`
  // TODO: fix timezone offset
  event.startDate = compose(setMinutes(minutes), setHours(hour))(date)

  return event
}

// this makes this file a module to TypeScript
export {}
