import compose from 'compose-function'
import { add } from 'date-fns/esm/fp'
import { range } from 'remeda'
import { removeTime } from './utils'

/**
 * custom element that provides infinite scrolling
 * so the user can navigate to any day.
 * contains `<single-day>` elements,
 * each of which contains all
 * the events planned for that day,
 * and allows new events to be created.
 */
export class DayScroller extends HTMLElement {
  constructor() {
    super()

    const shadow = this.attachShadow({ mode: 'open' })
    const yesterdayTodayTomorrow = range(-1, 2).map(
      compose(
        (d: Date) => d.toISOString(),
        removeTime,
        (n) => add({ days: n }, new Date()),
      ),
    )

    shadow.innerHTML = yesterdayTodayTomorrow
      .map((date: string) => `<single-day date="${date}"></single-day>`)
      .join('\n')
  }

  connectedCallback() {
    const days = this.shadowRoot!.querySelectorAll('single-day')
    const rect = days[1].getBoundingClientRect()
    window.scrollTo(0, rect.top)
  }
}

customElements.define('day-scroller', DayScroller)

// this makes this file a module to TypeScript
export {}
