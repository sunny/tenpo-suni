import { spring } from '@zanchi/wobble'

const css = `
  :host {
    align-items: center;
    background: #323636;
    bottom: -50px;
    box-shadow: 0 -8px 8px hwb(0.27turn 0% 94% / 0.28);
    display: grid;
    gap: 32px;
    grid-template-columns: 64px 0.9fr;
    height: 160px;
    left: 0;
    padding: 0 1rem 50px;
    position: fixed;
    width: 100%;
    z-index: 10;
  }

  .emoji-container {
    --size: 2.25rem;
    align-items: center;
    display: flex;
    font-size: 2.25rem;
    height: var(--size);
    position: relative;
    width: var(--size);
  }

  .emoji-container::after {
    content: '\\FF5C';
  }

  emoji-picker {
    --category-emoji-size: 4vw;
    --emoji-size: 5vw;
    --num-columns: 6;
    bottom: 0;
    font-size: 0.75rem;
    left: -0.5rem;
    max-width: 95vw;
    position: absolute;
  }

  figure {
    font-size: inherit;
    margin: 0;
  }

  input {
    background: transparent;
    border: 0;
    border-bottom: 2px solid #444;
    color: #fff;
    height: 52px;
  }
`

/**
 * returns how many ms the number of frames takes
 * if the animation runs at 60 fps.
 *
 * @example
 * ```
 * framesTo60fps(60) // 1
 * framesTo60fps(1) // 16.667
 * ```
 */
const framesToFps = (fps: number) => (frames: number) => (frames * 1000) / fps
const framesTo60Fps = framesToFps(120)

const KEYFRAMES = Array.from(
  spring(
    300,
    0,
    0,
    { mass: 0.86, stiffness: 231, damping: 19 },
    { timeStep: 1000 / 120 },
  ),
).map((v) => ({
  transform: `translateY(${v.toFixed(1)}px)`,
}))

export class EventEditor extends HTMLElement {
  emojiPickerOpen = false
  eventEmoji = '😀'

  constructor() {
    super()
    const shadow = this.attachShadow({ mode: 'open' })

    this.render()
    const style = document.createElement('style')
    style.textContent = css
    shadow.appendChild(style)
    this.slideIn()
  }

  get eventId(): string {
    return this.getAttribute('event-id') as string
  }

  set eventId(value: string) {
    this.setAttribute('event-id', value)
  }

  slideIn() {
    this.animate(KEYFRAMES, {
      duration: framesTo60Fps(KEYFRAMES.length),
    })
  }

  emoji = () => {
    const div = document.createElement('div')
    div.classList.add('emoji-container')

    if (this.emojiPickerOpen) {
      const emojiPicker = document.createElement('emoji-picker')
      emojiPicker.addEventListener('emoji-click', (e) => {
        this.eventEmoji = e.detail.unicode as string

        this.emojiPickerOpen = false
        this.render()
      })

      div.append(emojiPicker)
      return div
    }

    const figure = document.createElement('figure')
    figure.textContent = this.eventEmoji
    figure.addEventListener('click', () => {
      this.emojiPickerOpen = true
      this.render()
    })

    div.append(figure)
    return div
  }

  input = () => {
    const i = document.createElement('input')
    i.placeholder = 'new block'

    return i
  }

  render() {
    const elements = this.shadowRoot?.querySelectorAll(':host > :not(style)')
    elements?.forEach((element) => this.shadowRoot?.removeChild(element))
    this.shadowRoot?.append(this.emoji(), this.input())
  }
}

customElements.define('event-editor', EventEditor)

// this makes this file a module to TypeScript
export {}
