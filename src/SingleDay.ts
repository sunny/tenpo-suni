import h from 'hyperscript'
import { range } from 'remeda'
import { DATE_H1_HEIGHT, HOUR_HEIGHT } from './constants'
import { createEvent } from './DayEvent'
import { EventEditor } from './EventEditor'

import { day, hour, nearestAncestor, floorToNearest } from './utils'

/**
 * return a list of 24 hours for a given date
 */
const hours = (date: Date) =>
  range(0, 24).map((n) =>
    h('li', { 'data-hour': n }, [
      h(
        'p',
        hour(
          new Date(date.getFullYear(), date.getMonth(), date.getDay(), n, 0, 0),
        ),
      ),
      h('div', h('div')),
    ]),
  )

const css = `
* {
  box-sizing: border-box;
}

:host {
  display: grid;
  grid-template-rows: max-content 1fr;
  position: relative;
}

ol {
  align-items: stretch;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: start;
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
}

li {
  display: grid;
  grid-template-columns: 4.1rem 1fr;
  height: ${HOUR_HEIGHT}px;
  place-items: center;
  position: relative;
  justify-items: stretch;
}

li:first-of-type::after {
  background: #181820;
  content: '';
  height: 1.25rem;
  position: absolute;
  left: 0;
  top: -1.25rem;
  width: 4.1rem;
  z-index: 2;
}

li:first-of-type > div {
  border-top: 1px solid #22242f;
}

li > div {
  background: #0d0e12;
  border-bottom: 1px solid #22242f;
  display: grid;
  grid-template-rows: repeat(4, 1fr);
  height: 100%;
}

li > div > div {
  border-bottom: 1px solid #202020;
  grid-row: 1 / 3;
}

li > p {
  border-right: 3px solid #22242f;
  font-size: 1.2rem;
  place-self: start;
  text-align: center;
  transform: translateY(-50%);
  width: 100%;
  z-index: 3;
}

h1 {
  align-items: center;
  background: #0d0e12;
  display: flex;
  font-size: 1.5rem;
  height: ${DATE_H1_HEIGHT}rem;
  margin: 0;
  padding: 0 1rem 1.25rem;
}

p {
  margin: 0;
}
`

const showEditor = (eventId: string) => {
  const editor = document.createElement('event-editor') as EventEditor
  editor.eventId = eventId
  document.body.appendChild(editor)
}

class SingleDay extends HTMLElement {
  hourList: HTMLOListElement

  constructor() {
    super()
    const date = this.date
    const shadow = this.attachShadow({ mode: 'open' })

    this.hourList = h('ol.hour-list', { ariaHidden: 'true' }, hours(date))
    const header = h('h1', day(this.date))
    shadow.append(header, this.hourList)

    const style = document.createElement('style')
    style.textContent = css
    shadow.appendChild(style)
  }

  get date() {
    const date = this.getAttribute('date')!
    return new Date(date)
  }

  set date(value: Date) {
    this.setAttribute('date', value.toString())
  }

  connectedCallback() {
    this.hourList.addEventListener('mousedown', (e: MouseEvent) => {
      let { clientY } = e
      const target = e.target
      const li = nearestAncestor('li')(target as HTMLElement)
      if (!li) return
      // TODO: emit event here

      let { bottom, top } = li.getBoundingClientRect()
      clientY -= top
      bottom -= top

      const percentage = clientY / bottom
      const clampedPercentage = floorToNearest(0.25)(percentage)
      let hour = Number(li.dataset.hour as string)
      let minutes = 60 * clampedPercentage

      if (minutes === 60) {
        hour += 1
        minutes = 0
      }
      this.addDayEvent(hour, minutes)
    })
  }

  addDayEvent(hour: number, minutes: number) {
    const event = createEvent(hour, minutes, this.date)
    this.shadowRoot?.appendChild(event)
    showEditor(event.id)
  }
}

customElements.define('single-day', SingleDay)

// this makes this file a module to TypeScript
export {}
