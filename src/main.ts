import 'emoji-picker-element'

import './DayEvent'
// this is the main component of the app.
// it defines the `<day-scroller>` tag used in the HTML.
import './DayScroller'
import './EventEditor'
import './SingleDay'

import './style.css'
