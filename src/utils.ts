import compose from 'compose-function'
import {
  format,
  setHours,
  setMilliseconds,
  setMinutes,
  setSeconds,
} from 'date-fns/esm/fp'

/**
 * display the given date as a day.
 */
export const day = format('MMMM do')

/**
 * given a string `'prop'`,
 * return `obj[prop]`.
 */
export const getProp =
  <T>(prop: keyof T) =>
  (obj: T) =>
    obj[prop]

/**
 * returns the nearest ancestor element of type `name`.
 *
 * @example
 * ```
 * const nearestOl = nearestAncestor('ol')
 * const li = document.querySelector('li')
 * nearestOl(li) // <ol>...</ol>
 * ```
 */
export const nearestAncestor =
  (name: string) =>
  (element: HTMLElement): HTMLElement | null => {
    if (element.parentElement == null) {
      return null
    }
    if (element.parentElement.nodeName === name.toUpperCase()) {
      return element.parentElement
    }
    return nearestAncestor(name)(element.parentElement)
  }

/**
 * display a date's hour and minutes.
 *
 * @example
 * ```
 *   hour(new Date()) // '14:00'
 * ```
 */
export const hour: (date: Date) => string = format('HH:mm')

/**
 * given a `value`, floor it to the nearest given `interval`.
 *
 * @example
 * ```
 * floorToNearest(5)(23) // 20
 * floorToNearest(0.25)(1.23) // 1
 * ```
 */
export const floorToNearest = (interval: number) => (value: number) =>
  Math.floor(value / interval) * interval

/**
 * given a date, returns the same date
 * with the time zeroed out.
 *
 * @example
 * ```
 * const date = new Date() // 2022-08-14T04:41:13.997Z
 * removeTime(date) // 2022-08-14T00:00:00.000Z
 * ```
 */
export const removeTime: (date: Date) => Date = compose(
  setHours(0),
  setMinutes(0),
  setSeconds(0),
  setMilliseconds(0),
)
